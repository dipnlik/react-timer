import React from 'react'
import { NavLink } from 'react-router-dom'
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import logo from '../logo.svg'

export default class NavigationBar extends React.Component {
    render() {
        return (
            <Navbar inverse fluid collapseOnSelect className="NavigationBar">
                <Navbar.Header>
                    <Navbar.Brand>
                        <NavLink to="/">
                            <img src={logo} alt="" />
                            react-timer
                        </NavLink>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to="/timer"><NavItem>Timer</NavItem></LinkContainer>
                        <LinkContainer to="/countdown"><NavItem>Countdown</NavItem></LinkContainer>
                    </Nav>
                    <Navbar.Text pullRight>
                        <Navbar.Link href="http://about.me/dipnlik" target="_blank">Created by dipnlik</Navbar.Link>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}
