import React from 'react'
import Clock from './Clock'
import Controls from './Controls'

export default class Timer extends React.Component {
    state = { count: 0, status: 'stopped' }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.state.status !== prevState.status) {
            switch (this.state.status) {
                case 'started':
                    this.startTimer()
                    break
                case 'paused':
                    clearInterval(this.timer)
                    break
                case 'stopped':
                    clearInterval(this.timer)
                    this.setState({ count: 0 })
                    break
                default:
                    break
            }
        }
    }

    startTimer = () => {
        this.timer = setInterval(() => {
            this.setState({ count: this.state.count + 1 })
        }, 1000)
    }

    handleStatusChange = (status) => {
        this.setState({ status })
    }

    componentWillUnmount = () => {
        clearInterval(this.timer)
    }

    render() {
        const { count, status } = this.state
        return (
            <div className="Timer">
                <h1>Timer</h1>
                <Clock totalSeconds={count} />
                <Controls status={status} onStatusChange={this.handleStatusChange} />
            </div>
        )
    }
}
