import React from 'react'

export default class CountdownForm extends React.Component {
    onSubmit = (e) => {
        e.preventDefault()
        const strSeconds = this.refs.seconds.value

        if (strSeconds.match(/^\d+$/)) {
            this.refs.seconds.value = ''
            this.props.onSetCountdown(parseInt(strSeconds, 10))
        }
    }

    render() {
        return (
            <div className="CountdownForm row">
                <div className="col-xs-6 col-xs-offset-3">
                    <form ref="form" onSubmit={this.onSubmit}>
                        <div className="input-group">
                            <input type="text" className="form-control" ref="seconds" placeholder="Enter time in seconds" />
                            <span className="input-group-btn">
                                <input type="submit" className="btn btn-primary" />
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}
