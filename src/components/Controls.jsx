import React from 'react'
import PropTypes from 'prop-types'

export default class Controls extends React.Component {
    static get propTypes() {
        return {
            status: PropTypes.string.isRequired,
            onStatusChange: PropTypes.func
        }
    }

    onStatusChange = (newStatus) => {
        return this.props.onStatusChange(newStatus)
    }

    render() {
        const { status } = this.props

        const renderStartStopButton = () => {
            if (status === 'started') {
                return <button className="btn btn-default" onClick={() => this.onStatusChange('paused')}>Pause</button>
            } else {
                return <button className="btn btn-primary" onClick={() => this.onStatusChange('started')}>Start</button>
            }
        }

        return (
            <div className="Controls row">
                <div className="col-xs-6 col-xs-offset-3">
                    {renderStartStopButton()}{" "}
                    <button className="btn btn-danger" onClick={() => this.onStatusChange('stopped')}>Clear</button>
                </div>
            </div>
        )
    }
}
