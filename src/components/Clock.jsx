import React from 'react'
import PropTypes from 'prop-types'

export default class Clock extends React.Component {
    static get propTypes() {
        return {
            totalSeconds: PropTypes.number.isRequired
        }
    }

    static get defaultProps() {
        return {
            totalSeconds: 0
        }
    }

    formatSeconds = (totalSeconds) => {
        let hh = Math.floor(totalSeconds / 60)
        let mm = totalSeconds % 60
        if (hh < 10) { hh = '0' + hh }
        if (mm < 10) { mm = '0' + mm }
        return hh + ':' + mm
    }

    render() {
        const display = this.formatSeconds(this.props.totalSeconds)
        return (
            <div className="Clock">
                <span className="display">{display}</span>
            </div>
        )
    }
}
