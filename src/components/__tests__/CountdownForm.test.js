import React from 'react'
import ReactTestUtils from 'react-dom/test-utils'
import CountdownForm from '../CountdownForm'

describe('onSubmit', () => {
    const onSetCountdownMock = jest.fn()
    const countdownForm = ReactTestUtils.renderIntoDocument(
        <CountdownForm onSetCountdown={onSetCountdownMock} />
    )

    it('with valid data, should call onSetCountdown', () => {
        onSetCountdownMock.mockClear()
        countdownForm.refs.seconds.value = "123"
        ReactTestUtils.Simulate.submit(countdownForm.refs.form)
        expect(onSetCountdownMock).toBeCalledWith(123)
    })

    it('with invalid data, should not call onSetCountdown', () => {
        onSetCountdownMock.mockClear()
        countdownForm.refs.seconds.value = "abc"
        ReactTestUtils.Simulate.submit(countdownForm.refs.form)
        expect(onSetCountdownMock).not.toHaveBeenCalled()
    })
})
