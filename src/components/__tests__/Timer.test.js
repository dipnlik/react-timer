import React from 'react'
import ReactTestUtils from 'react-dom/test-utils'
import Timer from '../Timer'

describe('Timer', () => {
    it('defaults to stopped', () => {
        const timer = ReactTestUtils.renderIntoDocument(<Timer />)
        expect(timer.state.status).toBe('stopped')
        expect(timer.state.count).toBe(0)
    })
})

describe('handleStatusChange', () => {
    jest.useFakeTimers()

    it('should count up when status is started', () => {
        const timer = ReactTestUtils.renderIntoDocument(<Timer />)

        timer.handleStatusChange('started')
        jest.runTimersToTime(3000)

        expect(timer.state.status).toBe('started')
        expect(timer.state.count).toBe(3)
    })

    it('should pause when status is paused', () => {
        const timer = ReactTestUtils.renderIntoDocument(<Timer />)
        timer.setState({ status: 'started', count: 3 })

        timer.handleStatusChange('paused')
        jest.runTimersToTime(3000)

        expect(timer.state.status).toBe('paused')
        expect(timer.state.count).toBe(3)
    })

    it('should reset when status is stopped', () => {
        const timer = ReactTestUtils.renderIntoDocument(<Timer />)
        timer.setState({ status: 'started', count: 3 })

        timer.handleStatusChange('stopped')

        expect(timer.state.count).toBe(0)
        jest.runTimersToTime(3000)
        expect(timer.state.status).toBe('stopped')
        expect(timer.state.count).toBe(0)
    })
})
