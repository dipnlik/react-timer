import React from 'react'
import ReactTestUtils from 'react-dom/test-utils'
import Countdown from '../Countdown'

describe('handleSetCountdown', () => {
    jest.useFakeTimers()

    it('should set state to started and count down', () => {
        const countdown = ReactTestUtils.renderIntoDocument(<Countdown />)
        expect(countdown.state.status).toBe('stopped')

        countdown.handleSetCountdown(10)
        expect(countdown.state.status).toBe('started')
        expect(countdown.state.count).toBe(10)

        jest.runTimersToTime(3000)
        expect(countdown.state.count).toBe(7)

        // runTimersToTime is a bad name :(
        //   advanceTimers() would be more appropriate, for example
        jest.runTimersToTime(2000)
        expect(countdown.state.count).toBe(5)
    })

    it('should never count below zero', () => {
        const countdown = ReactTestUtils.renderIntoDocument(<Countdown />)
        countdown.handleSetCountdown(1)
        jest.runTimersToTime(2000)
        expect(countdown.state.count).toBe(0)
    })

    it('should pause countdown on pause status', () => {
        const countdown = ReactTestUtils.renderIntoDocument(<Countdown />)
        countdown.handleSetCountdown(3)
        countdown.handleStatusChange('paused')
        jest.runTimersToTime(1000)
        expect(countdown.state.count).toBe(3)
        expect(countdown.state.status).toBe('paused')
    })

    it('should stop countdown on stopped status', () => {
        const countdown = ReactTestUtils.renderIntoDocument(<Countdown />)
        countdown.handleSetCountdown(3)
        countdown.handleStatusChange('stopped')
        jest.runTimersToTime(1000)
        expect(countdown.state.count).toBe(0)
        expect(countdown.state.status).toBe('stopped')
    })
})
