import React from 'react'
import ReactTestUtils from 'react-dom/test-utils'
import Controls from '../Controls'

describe('render', () => {
    describe('renderStartStopButton', () => {
        test('when paused, should render Start button', () => {
            const controls = ReactTestUtils.renderIntoDocument(<Controls status='paused' />)
            const renderedButtons = ReactTestUtils.scryRenderedDOMComponentsWithTag(controls, 'button')
            expect(
                renderedButtons.map(b => b.innerHTML)
            ).toContain('Start')
        })

        test('when stopped, should render Start button', () => {
            const controls = ReactTestUtils.renderIntoDocument(<Controls status='stopped' />)
            const renderedButtons = ReactTestUtils.scryRenderedDOMComponentsWithTag(controls, 'button')
            expect(
                renderedButtons.map(b => b.innerHTML)
            ).toContain('Start')
        })

        test('when started, should render Pause button', () => {
            const controls = ReactTestUtils.renderIntoDocument(<Controls status='started' />)
            const renderedButtons = ReactTestUtils.scryRenderedDOMComponentsWithTag(controls, 'button')
            expect(
                renderedButtons.map(b => b.innerHTML)
            ).toContain('Pause')
        })
    })
})
