import React from 'react'
import ReactTestUtils from 'react-dom/test-utils'
import Clock from '../Clock'

describe('formatSeconds', () => {
    it('should include leading zeros', () => {
        const clock = ReactTestUtils.renderIntoDocument(<Clock />)
        expect(clock.formatSeconds(125)).toBe('02:05')
    })
})

describe('render', () => {
    it('should display clock', () => {
        const clock = ReactTestUtils.renderIntoDocument(<Clock totalSeconds={62} />)
        expect(
            ReactTestUtils.findRenderedDOMComponentWithClass(clock, 'display').innerHTML
        ).toBe('01:02')
    })
})
