import React from 'react'
import Clock from './Clock'
import CountdownForm from './CountdownForm'
import Controls from './Controls'

export default class Countdown extends React.Component {
    state = { count: 0, status: 'stopped' }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.state.status !== prevState.status) {
            switch (this.state.status) {
                case 'started':
                    this.startTimer()
                    break
                case 'stopped':
                    this.setState({ count: 0 })
                    clearInterval(this.timer)
                    break
                case 'paused':
                    clearInterval(this.timer)
                    break
                default:
                    break
            }
        }
    }

    startTimer = () => {
        this.timer = setInterval(() => {
            const newCount = this.state.count - 1
            this.setState({
                count: (newCount >= 0 ? newCount : 0)
            })
            if (newCount === 0) {
                this.setState({ status: 'stopped' })
            }
        }, 1000)
    }

    handleSetCountdown = (seconds) => {
        this.setState({ count: seconds, status: 'started' })
    }

    handleStatusChange = (newStatus) => {
        this.setState({ status: newStatus })
    }

    componentWillUnmount = () => {
        clearInterval(this.timer)
    }

    render() {
        const { count, status } = this.state

        const renderControlArea = () => {
            if (status !== 'stopped') {
                return <Controls status={status} onStatusChange={this.handleStatusChange} />
            } else {
                return <CountdownForm onSetCountdown={this.handleSetCountdown} />
            }
        }

        return (
            <div className="Countdown">
                <h1>Countdown</h1>
                <Clock totalSeconds={count} />
                {renderControlArea()}
            </div>
        )
    }
}
