import React, { Component } from 'react'
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import NavigationBar from './components/NavigationBar'
import Timer from './components/Timer'
import Countdown from './components/Countdown'

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

export default class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <NavigationBar />
                    <div className="container">
                        <Switch>
                            <Redirect exact from="/" to="/timer" />
                            <Route path="/timer" component={Timer} />
                            <Route path="/countdown" component={Countdown} />
                        </Switch>
                    </div>
                </div>
            </Router>
        )
    }
}
